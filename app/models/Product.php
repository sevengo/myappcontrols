<?php 

use Robbo\Presenter\PresentableInterface;

class Product extends Eloquent implements PresentableInterface  {
 
    protected $table = 'products';
 
    /*public function author()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }*/

    public function getPresenter()
    {
        return new ProductPresenter($this);
    }


    /**
     * get products images
     *
     * @return array of images
     */
    public function getImages()
    {
        $images = array();

        $dir = "/uploads/images/products/".$this->id."/arr/";

        foreach(glob(public_path().$dir."*.*") as $filename){
            $images[] = array(
                "dir" => $dir,
                "name" => basename($filename),
                "path" => $dir.basename($filename)
            );
        }

        return $images;
    }



}	