<?php 

namespace App\Models; 
use Eloquent;

class Order extends \Eloquent {
 
    protected $table = 'orders';
 
    public function product()
    {
        return $this->hasOne('Product');
    }
 
}	