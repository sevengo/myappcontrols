<?php

class AdminProductsController extends AdminController {


    /**
     * product Model
     * @var product
     */
    protected $product;

    /**
     * Inject the models.
     * @param product $product
     */
    public function __construct(Product $product)
    {
        parent::__construct();
        $this->product = $product;
    }

    /**
     * Show a list of all the product products.
     *
     * @return View
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/products/title.product_management');

        // Grab all the product products
        $products = $this->product;

        // Show the page
        return View::make('admin/products/index', compact('products', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        // Title
        $title = Lang::get('admin/products/title.create_a_new_product');

        // Show the page
        return View::make('admin/products/create_edit', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required',
            'price' => 'required',
            'version' => 'required',
            'description' => 'required',
            'file' => 'max:100000', //100   megas
            'summary' => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Create a new product
            $user = Auth::user();

            // Update the product data
            $this->product->name                 = Input::get('name');
            $this->product->price                = Input::get('price');
            $this->product->slug                 = Str::slug(Input::get('name'));
            $this->product->version              = Input::get('version');
            $this->product->description          = Input::get('description');
            $this->product->summary              = Input::get('summary');
            $this->product->tags                 = Input::get('tags');

            // Not required
            $this->product->video_url            = Input::get('video_url');
            $this->product->control_properties   = Input::get('meta-control_properties');
            $this->product->control_styles       = Input::get('meta-control_properties');

            if (Input::get('visible') == "checked")
                $this->product->visible = true;
            else
                $this->product->visible = false;

            // Was the product product created?
            if($this->product->save())
            {

                $to_re_save = false;
                //Getting main product image
                if (Input::hasFile('image')) {
                    $this->product->image = Image::upload(Input::file('image'), 'products/' . $this->product->id, "main-img");
                    $to_re_save = true;
                }

                //Getting product file
                if (Input::file('file')) {
                    //$extension = File::extension($file['name']);
                    //$directory = path('public').'uploads/'.sha1(time());
                    //$filename = sha1(time().time()).".{$extension}";
                    $file = Input::file('file');
                    $extension = $file->getClientOriginalExtension();
                    $filename  = $this->product->slug."_v".$this->product->version.".{$extension}";
                    $directory = storage_path().'/downloads/';

                    $upload_success = Input::file('file')->move($directory, $filename);
                    $this->product->digital_content_path = $filename;
                    $to_re_save = true;
                }

                if ($to_re_save)
                    $this->product->save();

                // Redirect to the new product product page
                return Redirect::to('admin/products/' . $this->product->id . '/edit')->with('success', Lang::get('admin/products/messages.create.success'));
            }


            // Redirect to the product product create page
            return Redirect::to('admin/products/create')->with('error', Lang::get('admin/products/messages.create.error'));
        }

        // Form validation failed
        return Redirect::to('admin/products/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $product
     * @return Response
     */
    public function getShow($product)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $product
     * @return Response
     */
    public function getEdit($product)
    {
        // Title
        $title = Lang::get('admin/products/title.product_update');

        // Show the page
        return View::make('admin/products/create_edit', compact('product', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $product
     * @return Response
     */
    public function postEdit($product)
    {

        // Declare the rules for the form validation
        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required',
            'price' => 'required',
            'version' => 'required',
            'description' => 'required',
            'file' => 'max:100000', //100   megas
            'summary' => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the product product data
            $product->name                 = Input::get('name');
            $product->price                = Input::get('price');
            $product->slug                 = Str::slug(Input::get('name'));
            $product->version              = Input::get('version');
            $product->description          = Input::get('description');
            $product->summary              = Input::get('summary');
            $product->tags                 = Input::get('tags');

            // Not required
            $product->video_url            = Input::get('video_url');
            $product->digital_content_path = Input::get('digital_content_path');
            $product->control_properties   = Input::get('meta-control_properties');
            $product->control_styles       = Input::get('meta-control_properties');


            if (Input::get('visible') == "checked")
                $product->visible = true;
            else
                $product->visible = false;

            //Getting main product image
            if (Input::hasFile('image'))
                $product->image = Image::upload(Input::file('image'), 'products/' . $product->id, "main-img");

            // Getting the upload product file
            if (Input::file('file')) {
                $file = Input::file('file');
                $extension = $file->getClientOriginalExtension();
                $filename  = $product->slug."_v".$product->version.".{$extension}";
                $directory = storage_path().'/downloads/';

                $upload_success = Input::file('file')->move($directory, $filename);
                $product->digital_content_path = $filename;
            }

            // Was the product product updated?
            if($product->save())
            {
                // Redirect to the new product product page
                return Redirect::to('admin/products/' . $product->id . '/edit')->with('success', Lang::get('admin/products/messages.update.success'));
            }

            // Redirect to the products product management page
            return Redirect::to('admin/products/' . $product->id . '/edit')->with('error', Lang::get('admin/products/messages.update.error'));
        }

        // Form validation failed
        return Redirect::to('admin/products/' . $product->id . '/edit')->withInput()->withErrors($validator);
    }


    /**
     * Download the specified resource from storage.
     *
     * @param $product
     * @return Response
     */
    public function getDownload($product)
    {

        $filename = $product->digital_content_path;
        $file_path = storage_path().'/downloads/'.$filename;
        //$file_path = "asdad";
        if (file_exists($file_path)) {
            return Response::download($file_path);
        }
        else {
            // Redirect to the products product management page
            return Redirect::to('admin/products/' . $product->id . '/edit')->with('error', Lang::get('admin/products/messages.file_not_found_error'));
        }
        // Title
        // $title = Lang::get('admin/products/title.product_delete');

        // Show the page
        //return View::make('admin/products/delete', compact('product', 'title'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $product
     * @return Response
     */
    public function postImageUpload($product) {

        $input = Input::all();
        $rules = array(
            'file' => 'image|max:10000',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->fails())
        {
            // Redirect to the products product management page
            return Response::make($validator->errors->first(), 400);
        }

        //Getting main product image
        if (Input::hasFile('file')) {

            $filename = sha1(time().time().$product->id);
            $image_url = Image::upload(Input::file('file'), 'products/'.$product->id.'/arr', $filename);

            die("aqui ".$image_url);
            if ($image_url) {
                return Response::json('success', 200);
            }
            else {
                return Response::json('error', 400);
            }
        }

        return Response::json('error', 400);
    }


    /**
     * Show products images
     *
     * @param $product
     * @return Response
     */
    public function getImages($product)
    {
        // Title
        $title = Lang::get('admin/products/title.product_images');

        $images = array();

        $dir = "/uploads/images/products/".$product->id."/arr/";

        foreach(glob(public_path().$dir."*.*") as $filename){
            $images[] = array(
                "dir" => $dir,
                "name" => basename($filename),
                "path" => $dir.basename($filename)
            );
        }

        return View::make('admin/products/images', compact('product', 'title', 'images'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $product
     * @return Response
     */
    public function getDelete($product)
    {
        // Title
        $title = Lang::get('admin/products/title.product_delete');

        // Show the page
        return View::make('admin/products/delete', compact('product', 'title'));
    }

    /**
     * Remove the specified image resource from storage.
     *
     * @param $product
     * @return Response
     */
    public function postDeleteImage($product)
    {
        $rules = array(
            'image_dir' => 'required',
            'image_name' => 'required'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);


        // Check if the form validates with success
        if ($validator->passes())
        {
            $image_name = Input::get('image_name');
            $image_dir = Input::get('image_dir');

            $path = public_path().$image_dir.$image_name;
            $path300x300 = public_path().$image_dir."300x300/".$image_name;

            if (file_exists($path) && File::delete($path)) {

                if (file_exists($path300x300)) {
                    File::delete($path300x300);
                }

                // Redirect to the new product product page
                return Redirect::to('admin/products/' . $product->id . '/images')->with('success', Lang::get('admin/products/messages.delete.success'));
            }
            else {
                return Redirect::to('admin/products/' . $product->id . '/images')->with('error', Lang::get('admin/products/messages.delete.error'));
            }
        }

        // Form validation failed
        return Redirect::to('admin/products/' . $product->id . '/images')->withInput()->withErrors($validator);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $product
     * @return Response
     */
    public function postDelete($product)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $product->id;
            $product->delete();

            //Deleting product file
            File::delete(storage_path()."/downloads/".$product->digital_content_path);
            //Deleting product images
            File::rmdir(public_path()."/uploads/images/products");

            // Was the product product deleted?
            $product = product::find($id);
            if(empty($product))
            {
                // Redirect to the product products management page
                return Redirect::to('admin/products')->with('success', Lang::get('admin/products/messages.delete.success'));
            }
        }
        // There was a problem deleting the product product
        return Redirect::to('admin/products')->with('error', Lang::get('admin/products/messages.delete.error'));
    }

    /**
     * Show a list of all the product products formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $products = Product::select(array('products.id', 'products.name', 'products.price', 'products.created_at'));

        return Datatables::of($products)


            ->add_column('actions', '<a href="{{{ URL::to(\'admin/products/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >{{{ Lang::get(\'button.edit\') }}}</a>
                <a href="{{{ URL::to(\'admin/products/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>
                <a href="{{{ URL::to(\'admin/products/\' . $id . \'/images\' ) }}}" class="btn btn-xs btn-info iframe">{{{ Lang::get(\'button.images\') }}}</a>
            ')

            ->remove_column('id')

            ->make();
    }

}