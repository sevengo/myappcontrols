<?php

class AdminContactsController extends AdminController
{

    /**
     * contact Model
     * @var contact
     */
    protected $contact;

    /**
     * Inject the models.
     * @param contact $contact
     */
    public function __construct(Contact $contact)
    {
        parent::__construct();
        $this->contact = $contact;
    }

    /**
     * Show a list of all the contact posts.
     *
     * @return View
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/contacts/title.contact_management');

        // Grab all the contact posts
        $contacts = $this->contact;

        // Show the page
        return View::make('admin/contacts/index', compact('contacts', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $contact
     * @return Response
     */
	public function getEdit($contact)
	{
        // Title
        $title = Lang::get('admin/contacts/title.contact_update');

        // Show the page
        return View::make('admin/contacts/create_edit', compact('contact', 'title'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $contact
     * @return Response
     */
	public function postEdit($contact)
	{
        // Declare the rules for the form validation
        $rules = array(
            'content' => 'required|min:3'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the contact post data
            $contact->content = Input::get('content');

            // Was the contact post updated?
            if($contact->save())
            {
                // Redirect to the new contact post page
                return Redirect::to('admin/contacts/' . $contact->id . '/edit')->with('success', Lang::get('admin/contacts/messages.update.success'));
            }

            // Redirect to the contacts post management page
            return Redirect::to('admin/contacts/' . $contact->id . '/edit')->with('error', Lang::get('admin/contacts/messages.update.error'));
        }

        // Form validation failed
        return Redirect::to('admin/contacts/' . $contact->id . '/edit')->withInput()->withErrors($validator);
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param $contact
     * @return Response
     */
	public function getDelete($contact)
	{
        // Title
        $title = Lang::get('admin/contacts/title.contact_delete');

        // Show the page
        return View::make('admin/contacts/delete', compact('contact', 'title'));
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param $contact
     * @return Response
     */
	public function postDelete($contact)
	{
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $contact->id;
            $contact->delete();

            // Was the contact post deleted?
            $contact = contact::find($id);
            if(empty($contact))
            {
                // Redirect to the contact posts management page
                return Redirect::to('admin/contacts')->with('success', Lang::get('admin/contacts/messages.delete.success'));
            }
        }
        // There was a problem deleting the contact post
        return Redirect::to('admin/contacts')->with('error', Lang::get('admin/contacts/messages.delete.error'));
	}

    /**
     * Show a list of all the contacts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $contacts = Contact::select(array('contacts.id', 'contacts.email', 'contacts.name', 'contacts.created_at'));

        return Datatables::of($contacts)


            ->add_column('actions', '<a href="{{{ URL::to(\'admin/contacts/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >{{{ Lang::get(\'button.edit\') }}}</a>
                <a href="{{{ URL::to(\'admin/contacts/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>
            ')

            ->remove_column('id')

            ->make();
    }

}
