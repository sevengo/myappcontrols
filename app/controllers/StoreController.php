<?php

class StoreController extends BaseController {

    /**
     * Contact Model
     * @var Contact
     */
    protected $contact;


    /**
     * Post Model
     * @var product
     */
    protected $post;

    /**
     * Product Model
     * @var product
     */
    protected $product;

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Inject the models.
     * @param Post $post
     * @param User $user
     */
    public function __construct(Contact $contact, User $user, Product $product, Post $post)
    {
        parent::__construct();

        $this->contact = $contact;
        $this->user = $user;
        $this->product = $product;
        $this->post = $post;
    }


    /**
     * View a blog post.
     *
     * @param  string  $slug
     * @return View
     * @throws NotFoundHttpException
     */
    public function getView($slug)
    {
        // Get this blog post data
        $post = $this->post->where('slug', '=', $slug)->first();

        // Check if the blog post exists
        if (is_null($post))
        {
            // If we ended up in here, it means that
            // a page or a blog post didn't exist.
            // So, this means that it is time for
            // 404 error page.
            return App::abort(404);
        }

        // Show the page
        return View::make('site/store/view_post', compact('post'));
    }


    public function getContact() {

        return View::make('site/store/contact');
    }

    public function postContact() {


        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required',
            'email' => 'required',
            'message' => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        if (!$validator->passes()) {
            // Form validation failed
            return Redirect::to('contact-us')->with('error', Lang::get('contact_us.form_error'));
        }

        $contact = new contact;
        $contact->name	        = Input::get('name');
        $contact->email	        = Input::get('email');
        //$contact->subject	    = Input::get('subject');
        $contact->message       = Input::get('message');

        $contact->save();

        $this->sendContactResponseEmail($contact);
        $this->sendContactNotificationEmail($contact);

        return Redirect::to('contact-us')->with('success', Lang::get('contact_us.contact_sent'));
    }

    /**
     * Send a contact notification email to website admins
     *
     * @param  object  $contact
     */
    private function sendContactNotificationEmail($contact) {

        // the data that will be passed into the mail view blade template

        $data = array(
            'message_txt'=> $contact->message,
            'name'  => $contact->name,
            'email' => $contact->email,
            'id'    => $contact->id,
            'subject' => $contact->investment
        );

        // use Mail::send function to send email passing the data and using the $user variable in the closure
        Mail::send('site.email.contact.notification', $data, function($message) use ($contact)
        {
            $message->from('management@myappcontrols.com', 'MyAppControls');
            //$message->to('contact@myappcontrols.com', 'MyAppControls')->subject('Contact');
            $message->to('marcus.oli.silva@gmail.com', 'MyAppControls')->subject('Contact');
        });

    }

    /**
     * Send a contact response email
     *
     * @param  object  $contact
     */
    private function sendContactResponseEmail($contact) {

        // the data that will be passed into the mail view blade template
        $data = array(
            'detail'=>'Your awesome detail here',
            'name' => $contact->name
        );

        // use Mail::send function to send email passing the data and using the $user variable in the closure
        Mail::send('site.email.contact.response', $data, function($message) use ($contact)
        {
            $message->from('contact@myappcontrols.com', 'MyAppControls');
            $message->to($contact['email'], $contact['name'])->subject('Contact MyAppControls');
        });

    }



	/**
 * Returns all the blog posts.
 *
 * @return View
 */
    public function getIndex()
    {
        // Get all the blog posts
        //$posts = $this->post->orderBy('created_at', 'DESC')->paginate(10);
        //$products = array();

        //$products_arr = $this->product->where('tags' ,'like', '%infographic%');

        $products = $this->product->all();

        //die();

        // Show the page
        return View::make('site/store/index', compact('products'));
    }

    /**
     * Show a product detail.
     *
     * @return View
     */
    public function getShowProduct($product)
    {

        //die(print_r($product));

        // Show the page
        return View::make('site/store/product', compact('product'));
    }

}
