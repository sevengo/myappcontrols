<?php

return array(

	'does_not_exist' => 'Product does not exist.',

	'create' => array(
		'error'   => 'Product was not created, please try again.',
		'success' => 'Product created successfully.'
	),

	'update' => array(
		'error'   => 'Product was not updated, please try again',
		'success' => 'Product updated successfully.'
	),

    'file_not_found_error' => 'This product does not have a downloadable file.',

    'file_upload_error' => 'Error uploading the file.',

	'delete' => array(
		'error'   => 'There was an issue deleting the product. Please try again.',
		'success' => 'The product was deleted successfully.'
	)

);
