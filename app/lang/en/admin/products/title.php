<?php

return array(

	'product_management'    => 'Product Management',
	'product_update'        => 'Update a product',
    'product_images'        => 'Product images',
	'product_delete'        => 'Delete a product',
	'create_a_new_product'  => 'Create a new product',

);