<?php

return array(

	'contact_management'    => 'Contacts Management',
	'contact_update'        => 'Update a Contact',
	'contact_delete'        => 'Delete a Contact',
	'create_a_new_contact'  => 'Create a New Contact',

);