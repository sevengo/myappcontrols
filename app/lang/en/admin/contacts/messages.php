<?php

return array(

	'does_not_exist' => 'Contact does not exist.',

	'create' => array(
		'error'   => 'Contact was not created, please try again.',
		'success' => 'Contact created successfully.'
	),

	'update' => array(
		'error'   => 'Contact was not updated, please try again',
		'success' => 'Contact updated successfully.'
	),

	'delete' => array(
		'error'   => 'There was an issue deleting the contact. Please try again.',
		'success' => 'The contact was deleted successfully.'
	)

);
