<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| User Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

    'form_error' => "Please fill required fields",
	'contact_sent' => "Your message has been sent. we'll be in touch with you soon",
    'contact_sent_email' => "We have received your email and will return as soon as possible"
);