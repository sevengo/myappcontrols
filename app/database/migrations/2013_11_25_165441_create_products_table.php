<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->float('price');

            $table->text('description');
            $table->text('summary');

            $table->text('slug');
            $table->string('version');
            $table->string('video_url')->nullable();

            $table->boolean("visible");

            $table->string('digital_content_path')->nullable();
            $table->string('image')->nullable();

            $table->string('tags')->nullable();


            $table->text('control_properties')->nullable();
            $table->text('control_styles')->nullable();

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
