<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {

            $table->increments('id');
            $table->string('order_status')->nullable();

            //relationship
            $table->integer('user_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            //$table->foreign('user_id')->references('id')->on('users');
            //$table->foreign('product_id')->references('id')->on('products');

            //paypal info
			$table->string('txn_id');
			$table->float('mc_gross');
            $table->string('payer_email');

            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
