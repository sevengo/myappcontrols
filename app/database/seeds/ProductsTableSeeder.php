<?php
 
class ProductsTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('products')->delete();

        $product = new product;
        $product->name = "Infographic 2";
        $product->price = 4.99;
        $product->summary = "Second product Column chart";
        $product->description = "First product Infographic";
        $product->version = "1";
        $product->tags = "infographic";
        $product->slug = "infographic-2";
        $product->visible = true;
        $product->save();

        $product = new product;
        $product->name = "Column chart";
        $product->summary = "Second product Column chart";
        $product->description = "Second product Column chart";
        $product->version = "1";
        $product->price = 4.99;
        $product->tags = "chart";
        $product->slug = "column-chart";
        $product->visible = true;
        $product->save();

        $product = new product;
        $product->name = "Line chart";
        $product->summary = "Third product Line chart";
        $product->description = "Third product Line chart";
        $product->version = "1";
        $product->price = 4.99;
        $product->tags = "chart";
        $product->slug = "line-chart";
        $product->visible = true;
        $product->save();
    }
 
}