@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	<!-- Tabs -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
		</ul>
	<!-- ./ tabs -->

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($contact)){{ URL::to('admin/contacts/' . $contact->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">

                <!-- product name -->
				<div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="name">Contact Name</label>
						<input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('title', isset($contact) ? $contact->name : null) }}}" />
						{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ product name -->

                <!-- Content -->
                <div class="form-group {{{ $errors->has('message') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="content">Description</label>
                        <textarea class="textarea form-control full-width " name="description" value="description" rows="10">{{{ Input::old('description', isset($contact) ? $contact->message : null) }}}</textarea>
                        {{{ $errors->first('message', '<span class="help-inline">:message</span>') }}}
                    </div>
                </div>
                <!-- ./ content -->

			</div>
			<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Cancel</element>
				<!-- <button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-success">Update</button> -->
			</div>
		</div>
		<!-- ./ form actions -->
	</form>


    </div>

@stop
