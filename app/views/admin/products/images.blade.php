@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
<!-- Tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-general" data-toggle="tab">Images</a></li>
</ul>
<!-- ./ tabs -->

{{-- Edit Blog Form --}}

    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- General tab -->
        <div class="tab-pane active" id="tab-general">
            <!-- product name -->
            <div class="form-group">
                @foreach ($images as $image)
                <div class="col-md-12">
                    <img src="{{ Image::resize($image['path'], 300, 300) }}" alt="" />
                    <br>
                    <br>
                    {{-- Delete Image Form --}}
                    <form class="form-horizontal" method="post" action="{{ URL::to("admin/products/".$product->id."/images/delete") }}" autocomplete="off">
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                        <!-- ./ csrf token -->
                        <input type="hidden" name="image_dir" value="{{{ $image['dir'] }}}" />
                        <input type="hidden" name="image_name" value="{{{ $image['name'] }}}" />

                        <!-- Form Actions -->
                        <div class="control-group">
                            <div class="controls">
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </div>
                        </div>
                        <!-- ./ form actions -->
                    </form>
                </div>
                <hr>
                @endforeach
            </div>
            <!-- ./ product name -->

        </div>
        <!-- ./ general tab -->
    </div>
    <!-- ./ tabs content -->

</form>


</div>

@stop
