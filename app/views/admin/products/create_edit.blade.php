@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	<!-- Tabs -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
            <li><a href="#tab-description" data-toggle="tab">Description</a></li>
			<li><a href="#tab-digital-content" data-toggle="tab">Digital Content</a></li>
		</ul>
	<!-- ./ tabs -->

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($product)){{ URL::to('admin/products/' . $product->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- product name -->
				<div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="name">Product Name</label>
						<input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('title', isset($product) ? $product->name : null) }}}" />
						{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ product name -->

                <!-- product price -->
                <div class="form-group {{{ $errors->has('price') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="name">Price</label>
                        <input class="form-control" type="text" name="price" id="price" value="{{{ Input::old('price', isset($product) ? $product->price : null) }}}" />
                        {{ $errors->first('price', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <!-- ./ product price -->

                <!-- product tags -->
                <div class="form-group {{{ $errors->has('tags') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="name">Tags</label>
                        <input class="form-control" type="text" name="tags" id="tags" value="{{{ Input::old('tags', isset($product) ? $product->tags : null) }}}" />
                        {{ $errors->first('tags', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <!-- ./ product tags -->

                <!-- product version -->
                <div class="form-group {{{ $errors->has('version') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="name">Version</label>
                        <input class="form-control" type="text" name="version" id="version" value="{{{ Input::old('version', isset($product) ? $product->version : null) }}}" />
                        {{ $errors->first('version', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <!-- ./ product version -->

                <!-- product version -->
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label" for="name">Visible</label>
                        @if (isset($product))
                            {{ Form::checkbox('visible', 'checked', $product->visible ? true:false) }}
                        @else
                        {{ Form::checkbox('visible', 'checked', true) }}
                        @endif
                    </div>
                </div>
                <!-- ./ product version -->


                <!-- summary -->
                <div class="form-group {{{ $errors->has('summary') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="content">Summary</label>
                        <textarea class="ckeditor form-control full-width " name="summary" value="description" rows="10">{{{ Input::old('summary', isset($product) ? $product->summary : null) }}}</textarea>
                        {{{ $errors->first('summary', '<span class="help-inline">:message</span>') }}}
                    </div>
                </div>
                <!-- ./ summary -->


			</div>
			<!-- ./ general tab -->

            <!-- Description tab -->
            <div class="tab-pane" id="tab-description">
                <!-- Content -->
                <div class="form-group {{{ $errors->has('description') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="content">Description</label>
                        <textarea class="ckeditor form-control full-width " name="description" value="description" rows="10">{{{ Input::old('description', isset($product) ? $product->description : null) }}}</textarea>
                        {{{ $errors->first('description', '<span class="help-inline">:message</span>') }}}
                    </div>
                </div>
                <!-- ./ content -->
            </div>
            <!-- ./ Description data tab -->

			<!-- Digital content tab -->
			<div class="tab-pane" id="tab-digital-content">
				<!-- Meta Title -->
				<div class="form-group">
					<div class="col-md-12">
                        <?php if (isset($product) && $product->digital_content_path != null) { ?>

                               <a href="{{ URL::to("admin/products/".$product->id."/download") }}">
                                    {{{ $product->digital_content_path }}}
                               </a>
                               <br>
                        <?php }?>
                        <label class="control-label" for="meta-title">File:</label>
                        <input class="form-control" type="file" name="file" id="file" />
					</div>

                    </br>

                    @if (isset($product) && $product->image)
                    <div class="col-md-12">
                        <img alt="" src="{{ Image::resize($product->image, 150, 150) }}"/>
                    </div>
                    @endif

                    <div class="col-md-12">
                        <label class="control-label" for="meta-title">Image:</label>
                        <input class="form-control" type="file" name="image" id="image" />
                    </div>


				</div>
				<!-- ./ meta title -->
			</div>
			<!-- ./ Digital content -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Cancel</element>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-success">Update</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>

    <!-- -->
    @if (isset($product))
    <h3>Images</h3>
    <form action="{{ URL::to("admin/products/".$product->id."/image-upload") }}" class="dropzone" id="my-awesome-dropzone">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <!-- ./ csrf token -->
    </form>
    </br>
    </br>
    @endif

    </div>

@stop
