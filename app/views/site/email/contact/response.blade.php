<?php
/**
 * Created by JetBrains PhpStorm.
 * User: marcusoliveira
 * Date: 30/10/13
 * Time: 15:25
 * To change this template use File | Settings | File Templates.
 */

?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Hi,  {{ $name }} </h2>

<div>
    <p> Nome: {{ Lang::get('contact/contact.contact_sent_email') }} </p>

    <br>
    Thanks for the contact,
    MyAppControls Team.
</div>

</body>
</html>