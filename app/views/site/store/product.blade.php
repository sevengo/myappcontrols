@extends('site.layouts.master')

@section('content')


<div class="jumbotron banner detail-banner">
    <div class="container">
        <h1>{{ $product->name }}</h1>
        <!-- <button type="button" class="btn btn-main-color">I want a new Property!</button> -->

        @if (Auth::check())
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="XU4H875B6S3A6">
            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
            <img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
        </form>
        @else
        <a href= '{{ URL::to("products/".$product->id."/show") }}' >
            <button type="button" class="btn btn-secondary-color">Add to cart ${{ $product->price }}</button>
        </a>
        @endif
    </div>
</div>

<div class="container product-detail">

    <div class="row">
        <div class="col-md-2">
            <div class="visual-info pull-left">
                <img src="{{ Image::thumb($product->image, 250)}}"/>

                <div class="container center justified-container ">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>

                    <hr />

                    <h5 class="">Price: <span class="badge">${{ $product->price}}</span></h5>
                    <h5 class="">Tags: <span class="badge">{{ $product->tags}}</span></h5>
                    <!--
                    <h5 class="">Downloads: <span class="badge">255</span></h5>
                    <h5 class="">Rating: <span class="badge">4,5</span></h5>
                    -->
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-1">

            <div class="info">

                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">

                        <?php
                        $first_image = true;
                        ?>
                        @foreach ($product->getImages() as $image)
                        <div class="item {{ $first_image ? 'active' : '' }}">
                            <center>
                                <img src="{{ Image::thumb($image['path'], 300) }}"/>
                            </center>
                            <!-- <div class="carousel-caption">
                                Teste
                            </div> -->
                        </div>

                        <?php
                        $first_image = false;
                        ?>
                        @endforeach
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>

                <h2>Description</h2><hr />
                {{ $product->description }}

                <h2>How to use it</h2><hr />

                <div class="video">
                    <iframe width="85%" height="360px" src="http://www.youtube.com/embed/MoXGJY1Iyjc?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
</div>