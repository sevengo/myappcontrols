@extends('site.layouts.master')

{{-- Web site Title --}}
@section('title')

@parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
	<h1>Contact us</h1>

    <p>Having problems with a control or just want to ask a question?
        Give us a shout, we’re here to help.</p>
</div>
<form class="form-horizontal" method="post" action="{{ URL::to('contact-us') }}" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <fieldset>
        <div class="form-group">
            <div class="col-lg-8">
                {{ Form::text('name', null,
                $attributes = array( 'class' => "form-control form-square required",
                'name' => "name",
                'id' => "name" ,
                'placeholder'=>"Name*")) }}
            </div>
        </div>


        <div class="form-group">
            <div class="col-lg-8">
                {{ Form::text('email', null,
                $attributes = array( 'class' => "form-control form-square required",
                'name' => "email",
                'id' => "email" ,
                'placeholder'=>"Email*")) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-8">
                {{ Form::textarea('message', null,
                $attributes = array( 'class' => "form-control form-square",
                'type'=> "message",
                'id' => "message" ,
                'rows' => "6" ,
                'placeholder'=>"Message")) }}
            </div>
        </div>


        <div class="form-group">
            <div class="col-lg-8">
                <center>
                    {{ Form::submit('Submit', array('class' => 'btn btn-background-color btn-lg')) }}
                </center>
            </div>
        </div>

        @if ( Session::get('success') )
        <div class="alert">{{ Session::get('success') }}</div>
        @endif

        @if ( Session::get('error') )
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif

        @if ( Session::get('notice') )
        <div class="alert">{{ Session::get('notice') }}</div>
        @endif

    </fieldset>
</form>

@stop
