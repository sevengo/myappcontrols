@extends('site.layouts.master')

@section('content')

<div class="jumbotron center banner">
    <h1>MyAppControls</h1>
    <br>
    <p>Leave the UI headache for us</p>
</div>


<div class="container product-list">
    <h2>Infographics</h2>
    <hr/>
    <div class="row">
        @foreach ($products as $product)
        <div class="col-md-4 center box">
            <h3 class="left">{{ $product->name }}</h3>
            <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="#">Control</a></li>
                <li><a href="#">Video</a></li>
                <li><a href="#">Source Code</a></li>
            </ul>

            <div class="column-content">
                <div class="visual-info">
                    <img src="{{ Image::thumb($product->image, 150) }}"/>
                </div>
                <div class="info">
                    <p>{{ $product->summary }}</p>
                </div>

                <a href= '{{ URL::to("products/".$product->id."/show") }}' >
                <button type="button" class="btn btn-main-color">
                    More
                </button>
                </a>

                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="XU4H875B6S3A6">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
                </form>

                <!-- <button type="button" class="btn btn-secondary-color">
                    Add to cart {{ money_format('$%i', $product->price) }}
                </button> -->
            </div>
        </div>
        @endforeach
    </div>
</div>


<div class="container product-list">

    <h2>Infographics</h2>
    <hr />
    <div class="row">
        <div class="col-md-4 center box">
            <h3 class="left">Pie Chart</h3>
            <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="#">Control</a></li>
                <li><a href="#">Video</a></li>
                <li><a href="#">Source Code</a></li>
            </ul>

            <div class="column-content">
                <div class="visual-info">
                    <img src="{{ asset('site/assets/img/pie-chart.png') }}"/>
                </div>
                <div class="info">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                </div>
                <button type="button" class="btn btn-main-color">More</button>
                <button type="button" class="btn btn-secondary-color">Add to cart $1.99</button>
            </div>
        </div>

        <div class="col-md-4 center">
            <h3 class="left">Pie Chart</h3>
            <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="#">Control</a></li>
                <li><a href="#">Video</a></li>
                <li><a href="#">Source Code</a></li>
            </ul>
            <div class="column-content">
                <div class="visual-info">
                    <img src="{{ asset('site/assets/img/pie-chart.png') }}"/>
                    <!--
                    <iframe height="90%" src="http://www.youtube.com/embed/MoXGJY1Iyjc?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                    -->
                </div>
                <div class="info">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                </div>
                <button type="button" class="btn btn-main-color">More</button>
                <button type="button" class="btn btn-secondary-color">Add to cart $1.99</button>
            </div>
        </div>

        <div class="col-md-4 center">
            <h3 class="left">Pie Chart</h3>
            <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="#">Control</a></li>
                <li><a href="#">Video</a></li>
                <li><a href="#">Source Code</a></li>
            </ul>

            <div class="column-content">
                <div class="visual-info">
                    <img src="{{ asset('site/assets/img/pie-chart.png') }}"/>
                </div>
                <div class="info">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                </div>
                <button type="button" class="btn btn-main-color">More</button>
                <button type="button" class="btn btn-secondary-color">Add to cart $1.99</button>
            </div>
        </div>
    </div>
</div>




<div class="container product-list">
    <h2>Views</h2>
    <hr />

    <div class="row">
        <div class="col-md-2">
            <div class="visual-info pull-left">
                <img src="{{ asset('site/assets/img/temperature-control.png') }}" width="100%"/>
            </div>
        </div>
        <div class="col-md-8">
            <div class="info">
                <h3 class="left">Temperature Control</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
        </div>
        <div class="col-md-2">
            <h2><button type="button" class="btn btn-secondary-color">Add to cart $1.99</button></h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <div class="visual-info pull-left">
                <img src="{{ asset('site/assets/img/temperature-control.png') }}" width="100%"/>
            </div>
        </div>
        <div class="col-md-8">
            <div class="info">
                <h3 class="left">Temperature Control</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
        </div>
        <div class="col-md-2">
            <h2><button type="button" class="btn btn-secondary-color">Add to cart $1.99</button></h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <div class="visual-info pull-left">
                <img src="{{ asset('site/assets/img/temperature-control.png') }}" width="100%"/>
            </div>
        </div>
        <div class="col-md-8">
            <div class="info">
                <h3 class="left">Temperature Control</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
        </div>
        <div class="col-md-2">
            <h2><button type="button" class="btn btn-secondary-color">Add to cart $1.99</button></h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <div class="visual-info pull-left">
                <img src="{{ asset('site/assets/img/temperature-control.png') }}" width="100%"/>
            </div>
        </div>
        <div class="col-md-8">
            <div class="info">
                <h3 class="left">Temperature Control</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
        </div>
        <div class="col-md-2">
            <h2><button type="button" class="btn btn-secondary-color">Add to cart $1.99</button></h2>
        </div>
    </div>
</div>




<div class="container product-list">
    <h2>Productivity</h2>
    <hr />

    <div class="row">
        <div class="col-md-6">
            <div class="row-content">
                <div class="visual-info pull-left">
                    <img src="{{ asset('site/assets/img/line-chart.png') }}"/>
                </div>
                <div class="info">
                    <h3 class="left">Line Chart</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                </div>
                <button type="button" class="btn btn-main-color">More</button>
                <button type="button" class="btn btn-secondary-color">Add to cart $1.99</button>
            </div>
        </div>

        <div class="col-md-6">
            <div class="row-content">
                <div class="visual-info pull-left">
                    <img src="{{ asset('site/assets/img/line-chart.png') }}"/>
                </div>
                <div class="info">
                    <h3 class="left">Line Chart</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                </div>
                <button type="button" class="btn btn-main-color">More</button>
                <button type="button" class="btn btn-secondary-color">Add to cart $1.99</button>
            </div>
        </div>
    </div>
</div>

@stop

