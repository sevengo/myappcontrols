<!DOCTYPE html>
<html lang="en"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Facebook meta tags -->
    <meta property="og:title" content="MyAppControls - Beautiful IOS app controls"/>
    <meta property="og:description" content="Da sua cabeça para o smartphone! Valide suas idéias de apps de forma barata e eficiente."/>
    <meta property="og:image" content="{{ asset('assets/img/7go-fb-logo-300.png') }}"/>
    <meta property="og:url" content="http://www.sevengo.com.br/"/>
    <!-- End Facebook meta tags -->

    <!-- Facebook conversion pixel -->
    <script type="text/javascript">
        var fb_param = {};
        fb_param.pixel_id = '6010597422878';
        fb_param.value = '0.00';
        fb_param.currency = 'USD';
        (function(){
            var fpw = document.createElement('script');
            fpw.async = true;
            fpw.src = '//connect.facebook.net/en_US/fp.js';
            var ref = document.getElementsByTagName('script')[0];
            ref.parentNode.insertBefore(fpw, ref);
        })();
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6010597422878&amp;value=0&amp;currency=USD" /></noscript>
    <!-- End Facebook conversion pixel -->

    <title>MyAppControls | Leave the UI headache for us</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/img/speech_bubble-128.png') }}">


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap-theme.min.css">

    <!-- Custom CSS -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-fixed-top navbar-custom" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{{ URL::to('') }}}">MyAppControls</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
                <li {{ (Request::is('/') ? ' class="active"' : '') }} >
                    <a  href="{{{ URL::to('') }}}" class="active">
                        Home
                    </a>
                </li>
                <li><a href="{{{ URL::to('contact-us') }}}">Contact-us</a></li>
                <li><a href="{{{ URL::to('faq') }}}">FAQ</a></li>
                <li><a href="{{{ URL::to('license') }}}">License</a></li>
                <li>
                    <a href="#">
                        <span class=".glyphicon .glyphicon-adjust"></span>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right user-menu">

                @if (Auth::check())
                    @if (Auth::user()->hasRole('admin'))
                    <li><a href="{{{ URL::to('admin') }}}">Admin Panel</a></li>
                    @endif
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Logged in as {{{ Auth::user()->username }}} <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{{ URL::to('user') }}}">Profile</a></li>
                            <li><a href="#">Purchases</a></li>
                            <li class="divider"></li>
                            <li><a href="{{{ URL::to('user/logout') }}}">Logout</a></li>
                        </ul>
                    </li>
                @else
                    <li {{ (Request::is('user/login') ? ' class="active"' : '') }}>
                        <a href="{{{ URL::to('user/login') }}}"> Login</a>
                    </li>
                    <li {{ (Request::is('user/register') ? ' class="active"' : '') }}>
                        <a href="{{{ URL::to('user/create') }}}">{{{ Lang::get('site.sign_up') }}}</a>
                    </li>
                @endif

                <!--
                <li>
                    <a href="#">Cart <span class="badge cart-badge"> 3</span></a>
                </li> -->
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>

@yield('content')

<nav class="navbar navbar-custom navbar-bottom" role="navigation">
    <div class="container">
        <p class="navbar-text pull-left">Copyright © Myapp Controls 2013</p>
    </div>
</nav>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
</body>
</html>