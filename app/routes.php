<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('user', 'User');
Route::model('comment', 'Comment');
Route::model('post', 'Post');
Route::model('role', 'Role');
Route::model('product', 'Product');
Route::model('contact', 'Contact');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{


    # Contact Management
    Route::get('contacts/{contact}/edit', 'AdminContactsController@getEdit')
        ->where('contact', '[0-9]+');
    Route::post('contacts/{contact}/edit', 'AdminContactsController@postEdit')
        ->where('contact', '[0-9]+');
    Route::get('contacts/{contact}/delete', 'AdminContactsController@getDelete')
        ->where('contact', '[0-9]+');
    Route::post('contacts/{contact}/delete', 'AdminContactsController@postDelete')
        ->where('contact', '[0-9]+');
    Route::controller('contacts', 'AdminContactsController');

    # Comment Management
    Route::get('comments/{comment}/edit', 'AdminCommentsController@getEdit')
        ->where('comment', '[0-9]+');
    Route::post('comments/{comment}/edit', 'AdminCommentsController@postEdit')
        ->where('comment', '[0-9]+');
    Route::get('comments/{comment}/delete', 'AdminCommentsController@getDelete')
        ->where('comment', '[0-9]+');
    Route::post('comments/{comment}/delete', 'AdminCommentsController@postDelete')
        ->where('comment', '[0-9]+');
    Route::controller('comments', 'AdminCommentsController');

    # Blog Management
    Route::get('blogs/{post}/show', 'AdminBlogsController@getShow')
        ->where('post', '[0-9]+');
    Route::get('blogs/{post}/edit', 'AdminBlogsController@getEdit')
        ->where('post', '[0-9]+');
    Route::post('blogs/{post}/edit', 'AdminBlogsController@postEdit')
        ->where('post', '[0-9]+');
    Route::get('blogs/{post}/delete', 'AdminBlogsController@getDelete')
        ->where('post', '[0-9]+');
    Route::post('blogs/{post}/delete', 'AdminBlogsController@postDelete')
        ->where('post', '[0-9]+');
    Route::controller('blogs', 'AdminBlogsController');

    # Products Management

    Route::get('products/{product}/download', 'AdminProductsController@getDownload')
        ->where('product', '[0-9]+');

    Route::get('products/{product}/images', 'AdminProductsController@getImages')
        ->where('product', '[0-9]+');

    Route::post('products/{product}/images/delete', 'AdminProductsController@postDeleteImage')
        ->where('product', '[0-9]+');

    Route::get('products/{product}/show', 'AdminProductsController@getShow')
        ->where('product', '[0-9]+');
    Route::get('products/{product}/edit', 'AdminProductsController@getEdit')
        ->where('product', '[0-9]+');
    Route::post('products/{product}/edit', 'AdminProductsController@postEdit')
        ->where('product', '[0-9]+');
    Route::get('products/{product}/delete', 'AdminProductsController@getDelete')
        ->where('product', '[0-9]+');
    Route::post('products/{product}/delete', 'AdminProductsController@postDelete')
        ->where('product', '[0-9]+');

    Route::post('products/{product}/image-upload', 'AdminProductsController@postImageUpload')
        ->where('product', '[0-9]+');

    Route::controller('products', 'AdminProductsController');

    # User Management
    Route::get('users/{user}/show', 'AdminUsersController@getShow')
        ->where('user', '[0-9]+');
    Route::get('users/{user}/edit', 'AdminUsersController@getEdit')
        ->where('user', '[0-9]+');
    Route::post('users/{user}/edit', 'AdminUsersController@postEdit')
        ->where('user', '[0-9]+');
    Route::get('users/{user}/delete', 'AdminUsersController@getDelete')
        ->where('user', '[0-9]+');
    Route::post('users/{user}/delete', 'AdminUsersController@postDelete')
        ->where('user', '[0-9]+');
    Route::controller('users', 'AdminUsersController');

    # User Role Management
    Route::get('roles/{role}/show', 'AdminRolesController@getShow')
        ->where('role', '[0-9]+');
    Route::get('roles/{role}/edit', 'AdminRolesController@getEdit')
        ->where('role', '[0-9]+');
    Route::post('roles/{role}/edit', 'AdminRolesController@postEdit')
        ->where('role', '[0-9]+');
    Route::get('roles/{role}/delete', 'AdminRolesController@getDelete')
        ->where('role', '[0-9]+');
    Route::post('roles/{role}/delete', 'AdminRolesController@postDelete')
        ->where('role', '[0-9]+');
    Route::controller('roles', 'AdminRolesController');

    # Admin Dashboard
    Route::controller('/', 'AdminDashboardController');
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */

// User reset routes
Route::get('user/reset/{token}', 'UserController@getReset')
    ->where('token', '[0-9a-z]+');
// User password reset
Route::post('user/reset/{token}', 'UserController@postReset')
    ->where('token', '[0-9a-z]+');
//:: User Account Routes ::
Route::post('user/{user}/edit', 'UserController@postEdit')
    ->where('user', '[0-9]+');

//:: User Account Routes ::
Route::post('user/login', 'UserController@postLogin');

# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'UserController');

//:: Application Routes ::

# Filter for detect language
//Route::when('contact-us','detectLang');

Route::get('contact-us', array('uses' => 'StoreController@getContact'));
Route::post('contact-us', array('uses' => 'StoreController@postContact'));

//Route::get('contact-us', array('before' => 'detectLang', 'uses' => 'StoreController@getContact'));

# Index Page - Last route, no matches
Route::get('/', array('uses' => 'StoreController@getIndex'));

Route::get('products/{product}/show',
    array('uses' => 'StoreController@getShowProduct'))
    ->where('product', '[0-9]+');;

# Posts - Second to last set, match slug
Route::get('{postSlug}', 'StoreController@getView');
//Route::post('{postSlug}', 'BlogController@postView');




